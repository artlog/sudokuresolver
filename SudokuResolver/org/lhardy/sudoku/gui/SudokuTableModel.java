package org.lhardy.sudoku.gui;

import javax.swing.table.AbstractTableModel;

import org.lhardy.sudoku.model.Cell;
import org.lhardy.sudoku.model.Table;

/**
 model for a sudoku table
 Table stores from 0 to 8 while model will display from 1 to 9.
 */
public class SudokuTableModel
extends AbstractTableModel
{

    private final static class DisplayCell
    {
	private Cell mCell;

	public DisplayCell(Cell pCell)
	{
	    mCell = pCell;
	}
	
	public String toString()
	{
	    return mCell.getPossibleValues().toStringShifted(1);
	}
    }
    
	Table table;
	
	public SudokuTableModel(Table pTable)
	{
		table=pTable;
	}
	
	public int getColumnCount()
	{
		return table.getColumns().size();
	}

	public int getRowCount() {
		return table.getRows().size();
	}

    public Object getValueAt(int pRow, int pColumn)
    {
	Cell cell = table.get(table.getRows().get(pRow), table.getColumns().get(pColumn));
	DisplayCell displayCell = new DisplayCell(cell);
	return displayCell;
    }

    public boolean isCellEditable(int row, int col)
    { 
    	return true; 
    }
    
    public void setValueAt(Object value, int pRow, int pColumn)
    { 
	System.out.println("(" + pColumn + "," + pRow +")=" + value);
	Cell cell = table.get(table.getRows().get(pRow), table.getColumns().get(pColumn)); 
	try {
	    // ["1"-"9"] -> [0-8] conversion
	    int i = Integer.parseInt((String) value) - 1;
	    cell.getPossibleValues().setValue(i);
	    fireTableCellUpdated(pRow, pColumn);
	}
	catch(Exception e)  {
	    cell.getPossibleValues().reset(false);
	}
    }

}
