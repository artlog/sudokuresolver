package org.lhardy.sudoku.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;

import javax.swing.border.Border;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import java.util.Scanner;
    
import org.lhardy.sudoku.SudokuResolver;
import org.lhardy.sudoku.SudokuReader;
import org.lhardy.sudoku.model.Table;

public class SudokuGui 
{

    /* sudoku table cells */
    JTable guiTable;
    Table table;
    JButton resolve;
    JFrame frame;
    JButton print;
    JButton restore;
    JButton fork;
    JButton randomizeOne;

    private JTable buildCellsTable(Table pTable)
    {
	int squarepixel=40;	
	Dimension squaredim=new Dimension(squarepixel,squarepixel);

	// didn't manage yet to get it fixed, not resizable.
	DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
	centerRenderer.setHorizontalAlignment( JLabel.CENTER );
	 
	centerRenderer.setMinimumSize(squaredim);
	centerRenderer.setPreferredSize(squaredim);	
	centerRenderer.setMaximumSize(squaredim);

	JTable jTable = new JTable(new SudokuTableModel(pTable))
	    {
		/*
		Code copied from https://coderanch.com/t/341895/java/Border-cells-JTable
Jeff Albertson 2006
		*/
		Color B = Color.BLACK;
		Color C = new Color(0,0,0,0);
		final Border[][] borders = {
		    {new ZoneBorder(B,C,C,B), new ZoneBorder(B,C,C,C), new ZoneBorder(B,B,C,C)},
		    {new ZoneBorder(C,C,C,B), new ZoneBorder(C,C,C,C), new ZoneBorder(C,B,C,C)},
		    {new ZoneBorder(C,C,B,B), new ZoneBorder(C,C,B,C), new ZoneBorder(C,B,B,C)}
		};
  
		public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		    Component result = super.prepareRenderer(renderer, row, column);
		    if (result instanceof JComponent) {
			((JComponent) result).setBorder(borders[row%3][column%3]);
		    }
		    return result;
		}
	    };

	{	    
	    TableModel tableModel = jTable.getModel();
	    for (int columnIndex = 0; columnIndex < tableModel.getColumnCount(); columnIndex++)
		{
		    TableColumn col = jTable.getColumnModel().getColumn(columnIndex);
		    col.setCellRenderer(centerRenderer);
		    col.setPreferredWidth(squarepixel);
		}
	    int s=(squarepixel+2)*tableModel.getColumnCount();
	    Dimension ps=new Dimension(s,s);
	    jTable.setPreferredSize(ps);

	}
	jTable.setRowHeight(squarepixel);
	// not honored, resized anyway
	jTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

	return jTable;
    }
	
	public SudokuGui(Table pTable)
	{
		frame = new JFrame("Sudoku");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		JPanel panel = new JPanel(new BorderLayout());

		table = pTable;
		
		guiTable = buildCellsTable(pTable);

		panel.add(guiTable,BorderLayout.CENTER);

		JPanel doButtons = new JPanel(new FlowLayout());
		panel.add(doButtons,BorderLayout.PAGE_END);

		randomizeOne = new JButton("RandomizeOne");
		randomizeOne.setEnabled(true);
		randomizeOne.setPreferredSize(new Dimension(100,40));

		randomizeOne.addActionListener(
					       new ActionListener() 
					       {
						   
						   public void actionPerformed(ActionEvent arg0)
						   {
						       System.out.println("RANDOMIZE ONE");
						       new Thread(){
							   public void run()
							   {
							       SudokuResolver resolver = new SudokuResolver(table);
							       resolver.randomizeOne();
							       System.out.println(table);
							       javax.swing.SwingUtilities.invokeLater(new Runnable() {
								       public void run() {
									   ((AbstractTableModel) guiTable.getModel()).fireTableDataChanged();
									   resolve.setEnabled(true);
								       }
								   });
	
							   }
						       }.start();
						   }
					       }	
					       );

		doButtons.add(randomizeOne);
		
		resolve = new JButton("Resolve");
		resolve.setEnabled(true);
		resolve.setPreferredSize(new Dimension(100,40));
		
		resolve.addActionListener(
				new ActionListener() 
				{

					public void actionPerformed(ActionEvent arg0)
					{
						System.out.println("RESOLVE");
						// TODO Auto-generated method stub
						if (true)
						{
							javax.swing.SwingUtilities.invokeLater(new Runnable() {
					            public void run() {
					            	resolve.setEnabled(false);
					            }
					        });
	
							new Thread(){
								public void run()
								{
									SudokuResolver resolver = new SudokuResolver(table);
									resolver.evaluateAll();
									System.out.println(table);
									javax.swing.SwingUtilities.invokeLater(new Runnable() {
							            public void run() {
							            	((AbstractTableModel) guiTable.getModel()).fireTableDataChanged();
							            	resolve.setEnabled(true);
							            }
							        });
	
								}
							}.start();
						}
					}	
				}
				);

		doButtons.add(resolve);
		
		JPanel persistsButtons = new JPanel(new FlowLayout());
		panel.add(persistsButtons,BorderLayout.LINE_END);

		restore = new JButton("Restore");
		restore.setEnabled(true);
		restore.setPreferredSize(new Dimension(100,40));

		restore.addActionListener(
				new ActionListener() 
				{

					public void actionPerformed(ActionEvent arg0)
					{
						System.out.println("RESTORE");
						if (true)
						{
						    javax.swing.SwingUtilities.invokeLater(new Runnable() {
					            public void run() {
					            	restore.setEnabled(false);					      
							JFileChooser filechooser = new JFileChooser();
							filechooser.showOpenDialog(frame);
							File file = filechooser.getSelectedFile();
							try {
							    SudokuReader reader = new SudokuReader(new Scanner(file).useDelimiter("\\Z").next(),9);
							    reader.readTableDump();
							    System.out.println(reader.getTable().toString());
							    final Table newTable = reader.getTable();
							    new Thread(){
								public void run()
								{
								    final Table sudoku = newTable;
							    	        javax.swing.SwingUtilities.invokeLater(new Runnable() {
							    	            public void run() {
							    	    			SudokuGui s = new SudokuGui(sudoku);
							    	            }
							    	        });
									fork.setEnabled(true);
									
								}
							    }.start();
							}
							catch(Exception e)
							    {
								e.printStackTrace(System.err);
							    }
							    							    restore.setEnabled(true);
						    }
							});
						}
					}	
				}
				);


		print = new JButton("Print");
		print.setEnabled(true);
		print.setPreferredSize(new Dimension(100,40));
		print.addActionListener(
				new ActionListener() 
				{

					public void actionPerformed(ActionEvent arg0)
					{
						System.out.println("PRINT");
						// TODO Auto-generated method stub
						if (true)
						{
							javax.swing.SwingUtilities.invokeLater(new Runnable() {
					            public void run() {
					            	print.setEnabled(false);
					      
							            	JFileChooser filechooser = new JFileChooser();
							            	filechooser.showOpenDialog(frame);
							            	File file = filechooser.getSelectedFile();
							            	if ( !file.exists())
							            	{
							            		try  {
							            			FileWriter fw = new FileWriter(file);
							            			fw.write(table.toString());
							            			fw.close();
							            		}
							            		catch(Exception e)
							            		{
							            			
							            		}
							            	}
							            	print.setEnabled(true);
							        }
							});
						}
					}	
				}
				);

		persistsButtons.add(print);
		persistsButtons.add(restore);

		fork = new JButton("Fork");
		fork.setEnabled(true);
		fork.setPreferredSize(new Dimension(100,40));
		fork.addActionListener(
				new ActionListener() 
				{

					public void actionPerformed(ActionEvent arg0)
					{
						System.out.println("FORK");
						// TODO Auto-generated method stub
						if (true)
						{
							javax.swing.SwingUtilities.invokeLater(new Runnable() {
					            public void run() {
					            	fork.setEnabled(false);

									new Thread(){
										public void run()
										{
											final Table sudoku = new Table(table);
							    	        javax.swing.SwingUtilities.invokeLater(new Runnable() {
							    	            public void run() {
							    	    			SudokuGui s = new SudokuGui(sudoku);
							    	            }
							    	        });
									        fork.setEnabled(true);
			
										}
									}.start();

							        }
							});
						}
					}	
				}
				);

		panel.add(fork,BorderLayout.LINE_START);

		frame.getContentPane().add(panel);

		frame.pack();

		frame.setVisible(true);
		
		//frame.setBounds(100, 100, 200, 200);

	}
	
}
