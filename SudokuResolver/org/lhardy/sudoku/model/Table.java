package org.lhardy.sudoku.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
intention was to be able to create a sudoku of any size.
most classical ones are : 2x2 and 9x9 ( ie 3x3 x 3x3 ).
 */
public class Table 
{
    // column, row and subtables are constraints over values, within one row, column or table only one same value can be used.
	private List<Column> mColumns;
	private List<Row> mRows;
	private List<SubTable> mSubTables;

	public static int root_square(int pValue)
	{
		// PL FIXME to be more generic
		return pValue == 4 ? 2 : (pValue == 9 ? 3 : 0) ; 
	}
	
	public Table(int pDifferentValues)
	{
		// construct table with columns, rows and subtable constraints 
		mRows = new ArrayList<Row>(pDifferentValues);
		mColumns = new ArrayList<Column>(pDifferentValues);
		ArrayList<Cell> defaultList = new ArrayList<Cell>(Arrays.asList(new Cell[pDifferentValues]));
		int root_square = root_square(pDifferentValues); 
		if (root_square > 0)
		{
			mSubTables = new ArrayList<SubTable>(pDifferentValues);
			for (int v =0; v <pDifferentValues; v++)
			{
				SubTable subTable = new SubTable(v);
				mSubTables.add(subTable);
				subTable.setConstrainedCells(new ArrayList<Cell>(defaultList));
			}
		}
		for (int v =0; v <pDifferentValues; v++)
		{
			Row row = new Row(v);
			mRows.add(row);
			row.setConstrainedCells(new ArrayList<Cell>(defaultList));

			Column column = new Column(v); 
			mColumns.add(column);
			column.setConstrainedCells(new ArrayList<Cell>(defaultList));

		}
		for (Column column : mColumns)
		{
			for (Row row : mRows)
			{
				
				Cell cell = new Cell(new ValueSet(pDifferentValues));
				row.setCell(column.getIndex(), cell);
				cell.addConstraint(row);
				column.setCell(row.getIndex(), cell);
				cell.addConstraint(column);
				if (mSubTables != null)
				{
					int index = column.getIndex() / root_square + ( row.getIndex() / root_square ) * root_square;
					SubTable subTable = mSubTables.get(index);
					subTable.set(column.getIndex()%root_square + ( row.getIndex()%root_square)*root_square,cell);
					cell.addConstraint(subTable);
				}
			}
		}
	}
	
	public Table(Table pOther)
	{
		this(pOther.getColumns().size());
		// Cells are shared then resetting all columns or all rows or all subtable is the same.
		for (Column column : pOther.getColumns())
		{
			Column thisColumn  = getColumns().get(column.getIndex());
			thisColumn.resetValues( column);
		}
	}

    public int getDifferentValues()
    {
	return mColumns.size();
    }

	public List<Column> getColumns()
	{
		return mColumns;
	}
	
	public List<Row> getRows()
	{
		return mRows;
	}
	
	public SubTable getSubTable(Row pRow, Column pColumn)
	{
		int root_square = root_square(mSubTables.size()); 
		int index = pColumn.getIndex() / root_square + ( pRow.getIndex() / root_square ) * root_square;
		return mSubTables.get(index);
	}
	
	List<SubTable> getSubTables()
	{
		return mSubTables;
	}
	
	public Cell get(Row pRow, Column pColumn)
	{
		return pRow.getAtColumn(pColumn);
	}
	
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append("rows:\n");
		for (Row row : mRows)
		{
			builder.append(row);
			builder.append("\n");
		}
		/*
		builder.append("columns:\n");
		for (Column column : mColumns)
		{
			builder.append(column);
			builder.append("\n");
		}
		if (mSubTables != null)
		{
			builder.append("subtables:\n");
			for (SubTable subtable : mSubTables)
			{
				builder.append(subtable);
				builder.append("\n");
			}
		}
		*/
		return builder.toString();
	}
}
