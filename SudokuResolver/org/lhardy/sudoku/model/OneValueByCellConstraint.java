package org.lhardy.sudoku.model;

import java.util.List;

public class OneValueByCellConstraint {

	private int index;
	
	private List<Cell> mConstrainedCells;

	protected OneValueByCellConstraint(final int pIndex)
	{
		index = pIndex;
	}
	
	public List<Cell> getConstrainedCells() {
		return mConstrainedCells;
	}

	public void setConstrainedCells(List<Cell> constrainedCells) {
		mConstrainedCells = constrainedCells;
	}
	
	public void setCell(int pIndex,final Cell pCell)
	{
		mConstrainedCells.set(pIndex, pCell);
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index)
	{
		this.index = index;
	}
	
	public void resetValues(OneValueByCellConstraint pToCopyFrom)
	{
		for (int i = 0; i < mConstrainedCells.size(); i++)
		{
			Cell thisCell = mConstrainedCells.get(i);
			thisCell.copyValues(pToCopyFrom.mConstrainedCells.get(i));
		}
	}
	
	@Override
	public String toString()
	{
		return "" + index + ":" + mConstrainedCells.toString();
	}
}
