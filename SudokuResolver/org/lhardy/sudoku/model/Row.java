package org.lhardy.sudoku.model;

public class Row
extends OneValueByCellConstraint
{
	
	public Row(int pIndex){
		super(pIndex);
	}

	public Cell getAtColumn( Column pColumn)
	{
		return getConstrainedCells().get(pColumn.getIndex());
	}

}
