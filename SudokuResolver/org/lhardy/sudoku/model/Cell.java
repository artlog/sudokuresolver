package org.lhardy.sudoku.model;

import java.util.ArrayList;
import java.util.List;

public class Cell {

	private ValueSet mPossibleValues;
	
	// to walk through changed values
	private Cell next;
	
	// constraints linked to that Cell
	private List<OneValueByCellConstraint> constraints;
	
	public Cell(final ValueSet pValueSet)
	{
		mPossibleValues = pValueSet;
	}
	
	public ValueSet getPossibleValues()
	{
		return mPossibleValues;
	}
	
	public void addConstraint(OneValueByCellConstraint pConstraint)
	{
		if (constraints == null)
		{
			constraints = new  ArrayList<OneValueByCellConstraint>();
		}
		constraints.add(pConstraint);
	}
	
	@Override
	public String toString()
	{
		return mPossibleValues.toString();
	}

	public Cell getNext()
	{
		return next;
	}

	public void setNext(Cell next)
	{
		this.next = next;
	}

	public List<OneValueByCellConstraint> getConstraints()
	{
		return constraints;
	}
	
	public void copyValues(Cell pCell)
	{
		mPossibleValues = new ValueSet(pCell.mPossibleValues);
	}

	public void setPossibleValues(ValueSet possibleValues) {
		mPossibleValues = possibleValues;
	}
}
