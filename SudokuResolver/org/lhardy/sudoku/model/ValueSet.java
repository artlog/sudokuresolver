package org.lhardy.sudoku.model;

import java.util.ArrayList;
import java.util.List;

/**
Well done , comments are wonderful !
Seems a litle weird construction...
**/

public class ValueSet {

    // how many bits encoded by int in set[]
	final static int DIGIT=16;
	private int [] set;
	final private int number;

    /** this set is non empty and contains ... */
	public ValueSet(int pNumber)
	{
		this(pNumber, false);
	}

    /** this set might be empty **/
	public ValueSet(int pNumber, boolean pEmpty)
	{
		number = pNumber;
		reset(pEmpty);
	}

    /**
if empty then bits in set will be 0, else they are 1 ( meaning can take all values ).
     */	
	public void reset(boolean pEmpty)
	{
		set = new int[(number/DIGIT) +1];
		for (int length=0 ; length < set.length; length++)
		{
			set[length] = pEmpty ? 0 : -1;
		}		
	}
	
	public ValueSet(ValueSet pOther)
	{
		number = pOther.number;
		set = new int[(number/DIGIT) + 1];
		for (int length=0 ; length < set.length; length++)
		{
			set[length] = pOther.set[length];
		}
	}

    /**
       mark value as possible.
     */
	public void addValue(int pValue)
	{
		set[pValue/DIGIT] |= 1 << pValue%DIGIT;
	}

    /**
       mark value as impossible
       return wether it did change something.
    */
	public boolean removeValue(int pValue)
	{
		int current = set[pValue/DIGIT];
		int newV = current & (~(1 << (pValue%DIGIT)));
		if (newV != current)
		{	
			set[pValue/DIGIT] = newV;
			return true;
		}
		return false;
	}

    /**
       keep only values belonging to both 
    */
	public boolean and(ValueSet pValue)
	{
		boolean changed = false;
		for (int i=0; i< ((set.length > pValue.set.length)? pValue.set.length : set.length); i++)
		{
			int newvalue = set[i] & pValue.set[i];
			if (newvalue != set[i])
			{
				set[i] = newvalue;
				changed  = true;
			}
		}
		return changed;
	}

    /**
       keep value belong to any
    */
	public void or(ValueSet pValue)
	{
		for (int i=0; i< ((set.length > pValue.set.length)? pValue.set.length : set.length); i++)
		{
			set[i] |= pValue.set[i];
		}		
	}
	
	public ValueSet complementary()
	{
		ValueSet complementary = new ValueSet(number);
		for (int i=0; i< set.length ; i++)
		{
			complementary.set[i] = ~set[i];
		}
		return complementary;
	}
	
	public boolean hasValue(int pValue)
	{
		return (set[pValue/DIGIT] & (1 << (pValue%DIGIT))) != 0;
	}
	
	public boolean setValue(int pValue)
	{
		int previous = set[pValue/DIGIT];
		int newV = 1 << (pValue%DIGIT);
		if ( newV != previous)
		{
			set[pValue/DIGIT] = newV;
			return true;
		}
		return false;
	}
	
	public List<Value> getValuesShifted(int pShift)
	{
	    List<Value> values = new ArrayList<Value>(number);
	    for (int i=0; i< set.length ; i++)
		{
		    for (int d=0; d <DIGIT; d++)
			{
			    int index = i*DIGIT + d;
			    if (index < number)
				{
				    if ((set[i] & (1 << d)) != 0)
					{
					    values.add(new Value((short)index, (short)pShift));
					}
				}
			}
		}
	    return values;
	}

    	public List<Value> getValues()
	{
	    return getValuesShifted(0);
	}

	public String toStringShifted(int i)
	{
		if ( set[0] == -1 )
		{
			return "";
		}
		else
		{
			List<Value> values = getValuesShifted(i);
			if (values.isEmpty())
			{
				return "Error";
			}
			else
			if (values.size() == 1)
			{
			    return values.get(0).toString();
			}
			else
			{
			    return values.toString();
			}
		}
	}

    	@Override
	public String toString()
	{
	    return toStringShifted(0);
	}


	public int getNumber()
	{
		return number;
	}
}
