package org.lhardy.sudoku.model;


public class SubTable
extends OneValueByCellConstraint
{

	public SubTable(int pIndex) {
		super(pIndex);
	}

	public Cell get(int pIndex)
	{
		return getConstrainedCells().get(pIndex);
	}
	
	public void set(int pIndex, final Cell pCell)
	{
		getConstrainedCells().set(pIndex, pCell);
	}
}
