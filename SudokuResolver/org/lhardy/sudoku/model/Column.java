package org.lhardy.sudoku.model;

public class Column
extends OneValueByCellConstraint
{

	public Column(int pIndex) {
		super(pIndex);
	}

	public Cell getAtRow(Row pRow)
	{
		return getConstrainedCells().get(pRow.getIndex());
	}
}
