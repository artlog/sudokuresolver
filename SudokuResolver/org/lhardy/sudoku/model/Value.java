package org.lhardy.sudoku.model;

public class Value {

    private final short mValue;
    /* shift for display since standrrd sudoku are from 1 to 9
    starts from shift.
    */
    private final short mShift;
	
    public Value(short pValue, short pShift)
    {
	mValue = pValue;
	mShift = pShift;
    }

    public Value(short pValue)
    {
	this(pValue,(short) 0);
    }

    @Override
    public String toString()
    {
	return "" + ( mValue + mShift );
    }

    public short getValue()
    {
	return mValue;
    }
}
