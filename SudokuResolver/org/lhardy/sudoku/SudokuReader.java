package org.lhardy.sudoku;

import java.lang.*;
import java.util.ArrayList;
import org.lhardy.sudoku.model.*;

/** will read a bare dump of Table 

as usual an adhoc parser not that smart
*/

public class SudokuReader {

    private boolean test = true;
    private int number;
    private int last;

    private char current = 0;

    private StringBuilder buffer = new StringBuilder();

    private String mInput;
    private int mIndex;

    int errors = 0;

    private Table mTable = null;
    
    public SudokuReader(String pInput, int rowcols)
    {
	test = (pInput == null);
	if ( ! test )
	    {
		mTable = new Table(rowcols);
	    }
	mInput = pInput;
	number = 0;
	last=rowcols;
    }

    public Table getTable()
    {
	return mTable;
    }

    private boolean isSeparator(char c)
    {
	return (c>0) && (c < 33);
    }
    
    private char readNextChar()
    {
	char c=readNextCharIntern();
	while ( isSeparator(c))
	    {
		c=readNextCharIntern();
	    }
	return c;
    }
    
    char readNextCharIntern()
    {
	if ( errors > 0 )
	    {
		return 0;
	    }
	if ( buffer.length() > 0)
	    {
		char  c = buffer.charAt(0);
		buffer.deleteCharAt(0);
		return c;
	    }
	else
	    {
		if ( mIndex < mInput.length())
		    {
			int i = mIndex;
			++ mIndex;
			return mInput.charAt(i);
		    }
	    }
	return 0;
    }

    private void error(String err)	
    {
	System.err.println(err);
	++errors;
    }

    private void nextNumber()
    {
	if (test)
	    {
		number = ( number + 7) % last;
	    }
    }
    
    private int readInt()
    {
	if ( test)
	    {
		int current=number;
		System.out.print(number);
		nextNumber();
		return current;
	    }
	char c = readNextChar();
	if (( c >= '0' ) && ( c <='9' ))
	    {
		return (int) (c - '0') ;
	    }
	else
	    {
		if ( c == ',' ) 
		    {
			// special case with empty content
			return -2;
		    }
		else if (  c == ']' )
		    {
			// special case with empty content at last element of a row
			return -3;
		    }
		error("expecting a number, got '" + c + "'");
		return -1;
	    }
    }
    
    private boolean isMatch(String s, int proba, int probb)
    {
	if (test)
	    {
		if ( proba > probb / 2 )
		    {
			System.out.print(s);
			return true;
		    }
		return false;
	    }
	boolean match = true;
	for (int im = 0; match && (im < s.length()); im ++)
	    {		
		char c = readNextChar();
		match = s.charAt(im) == c;
		buffer.append(c);
	    }
	if ( match )
	    {
		buffer = new StringBuilder();
	    }
	return match;	

    }

    private void match(String s)
    {
	if (test)
	    {
		System.out.print(s);
		return;
	    }
	for (int im = 0; im < s.length(); im ++)
	    {		
		char c = readNextChar();
		if ( s.charAt(im) != c )
		    {
			error("expecting '"+ s.charAt(im)+"' got '" +c+"'");
		    }
	    }
    }

    void readListUntil(String str,Cell cell)
    {	
	int a=0;
	a=readInt();
	if ( cell != null )
	    {
		// empty case not accepted for same cell
		if ( a < -1 )
		    {
			a=-1;
		    }
		cell.getPossibleValues().setValue(a);
	    }
	for (int j=1; (errors == 0) && (j<last);j++)
	    {	    
		if ( isMatch(str,j,last) )
		    {
			return;
		    }
		else if ( isMatch(",",(last-j),last) )
		    {
			if ( test)
			    {
				System.out.print(" ");
			    }
			a=readInt();
			// empty case not accepted for same cell
			if ( a < -1 )
			    {
				a=-1;
			    }
			if ( cell != null )
			    {
				cell.getPossibleValues().addValue(a);
			    }
		    }
		else
		    {
			error("expecting , or " + str + "" );
		    }
	    }
	error("EOF, was expecting " + str);
    }

    // returns last int read ( or -1 error, -2 ',' read, -3 ']' read
    int readRow(int i)
    {
	int a=0;	
	Row row = null;
	Cell cell = null;
	if ( ! test )
	    {
		row =mTable.getRows().get(i);
	    }
	for (int j=0; (errors == 0) && (j<last);j++)
	    {
		if ( ! test)
		    {
			Column column = mTable.getColumns().get(j);
			cell = row.getAtColumn(column);
			cell.getPossibleValues().reset(true);
		    }
		// either a number or [
		if ( isMatch("[",j,last) )
		    {
			readListUntil("]",cell);
		    }
		else
		    {
			a=readInt();
			if (cell != null)
			    {
				// accept empty cell, meaning all values are possible.
				if ( a > -2 )
				    {
					cell.getPossibleValues().setValue(a);
				    }
				else
				    {
					// empty cell means all possible values.
					/*
					for (int v =0; v<  mTable.getDifferentValues(); v++)
					    {
						cell.getPossibleValues().addValue(v);
					    }
					*/
					cell.getPossibleValues().reset(false);
				    }
				       
			    }
		    }
		if ( j != ( last-1))
		    {
			// consider ',' already matched.
			if ( a != -2 )
			    {
				match(",");
			    }
			if (test)
			    {
				System.out.print(" ");
			    }
		    }
	    }
	return a;
    }

    public void readTableDump()
    {
	match("rows:");
	if ( test)
	    {
		System.out.println("");
	    }
	for (int i=0; (errors == 0) && (i<last); i++)
	    {
		match(""+i+":");
		match("[");
		// if -3 consider ']' already matched.
		if ( readRow(i) > -3 )
		    {
			match("]");
		    }
		/*
		  rows are not coma (,) separated
		if ( i != (last-1))
		    {
			match(",");
		    }
		*/
		if ( test)
		    {
			System.out.println("");
		    }
	    }
    }

    public static void main(String pArgs[])
    {
	if ( pArgs.length == 0)
	    {
		SudokuReader reader = new SudokuReader(null,9);
		reader.readTableDump();
	    }
	else
	    {
		SudokuReader reader = new SudokuReader(pArgs[0],9);
		reader.readTableDump();
		System.out.println(reader.getTable().toString());
	    }
    }
}

