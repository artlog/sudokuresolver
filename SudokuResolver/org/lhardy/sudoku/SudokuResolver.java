package org.lhardy.sudoku;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.lhardy.sudoku.gui.SudokuGui;
import org.lhardy.sudoku.model.Cell;
import org.lhardy.sudoku.model.Column;
import org.lhardy.sudoku.model.OneValueByCellConstraint;
import org.lhardy.sudoku.model.Row;
import org.lhardy.sudoku.model.Table;
import org.lhardy.sudoku.model.Value;
import org.lhardy.sudoku.model.ValueSet;

public class SudokuResolver
{
    /**
       each queue keep list of open cells with a fixed number of different values.
       smaller index means a samller amount of possible values.
     **/
    private Queue priorityQueues[] = null;
    
    private class Queue
    {
	private Cell head=null;
	private Cell tail=null;

	public void enqueue(Cell pCell)
	{
	    // current implementation : a queue directly embedded into Cell.
	    if (pCell.getNext() != null)
		{
		    System.out.println("addInOpen skipped " + pCell);
		}
	    else
		{
		    if (tail != null)
			{
			    tail.setNext(pCell);
			}
		    else
			{
			    head = pCell;
			}
		    tail = pCell;
		    tail.setNext(tail);
		    System.out.println("addInOpen " + pCell);
		}
	}
	
        public Cell dequeue()
        {

	    if (head != null)
		{
		    Cell removeFromOpend = head;
		    if (removeFromOpend == tail)
			{
			    tail = null;
			    head = null;
			}
		    else
			{
			    if (removeFromOpend.getNext() == removeFromOpend)
				{
				    // should have been detected by dequeud = tail.
				    System.out.println("removeFromOpen queue corruption .2. " + tail + " " + removeFromOpend);
				    tail = head = null;
				}
			    else
				{
				    head = removeFromOpend.getNext();
				}
			}
		    removeFromOpend.setNext(null);
		    
		    System.out.println("removeFromOpend " + removeFromOpend);
		    
		    return removeFromOpend;
		}
	    else
		{
		    if ( (tail != head) || (tail != null) )
			{
			    System.out.println("removeFromOpen queue corruption .1. " + head + " " + tail);
			    tail = head = null;
			}
		}
	    return null;
	}
    }
	
    // select a strategy to get one element form all possible elements in open
    private static class SelectStrategy
    {
	
    }
	
	public SudokuResolver(Cell pFirst)
	{
	    // NOT SUPPORTED ANYMORE
	}
	
	public SudokuResolver(Table pTable)
	{	
	    priorityQueues = new Queue[pTable.getDifferentValues()+1];		
	    List<Column> columns = pTable.getColumns();
	    List<Row> rows = pTable.getRows();
	    for (Column column : columns)
		{
		    for (Row row : rows)
			{
			    addInOpen(pTable.get(row,column));
			}
		}
	}


    /** just pick one value within a possible set in a randomized manner.*/
    public void randomizeOne()
    {
	// well a very low random => pick the first value or the first cell with multiple values !
	// this alogirthm lead quickly to wrong path => introdcute this rule :
	// first select one cell with the minimal number of possible choices.
	
	ArrayList<Cell> toreaddInOpen = new ArrayList<Cell>();
	Cell toEvaluate = removeFromOpen();
	while( toEvaluate != null )
	    {
		toreaddInOpen.add(toEvaluate);
		ValueSet valueSet = toEvaluate.getPossibleValues();
		List<Value> currentValues = valueSet.getValues();
		if (currentValues.size() > 1)
		    {
			int v = currentValues.get(0).getValue();
			valueSet.reset(true);
			valueSet.setValue(v);
			// force stop
			toEvaluate = null;
		    }
		else
		    {
			toEvaluate = removeFromOpen();
		    }
	    }
	while (!toreaddInOpen.isEmpty())
	    {
		addInOpen(toreaddInOpen.remove(toreaddInOpen.size()-1));
	    }
	evaluateAll2();	
    }

    public void evaluateAll()
	{
		Cell toEvaluate = removeFromOpen();
		do
		{
		    evaluateUnique(toEvaluate);
		    evaluateMissing1(toEvaluate);
		    toEvaluate = removeFromOpen();
		} while (toEvaluate != null);
	}

    	public void evaluateAll2()
	{
		Cell toEvaluate = removeFromOpen();
		do
		{
		    evaluateUnique(toEvaluate);
		    evaluateMissing2(toEvaluate);
		    toEvaluate = removeFromOpen();
		} while (toEvaluate != null);
	}

	private void evaluateUnique(Cell pCell)
	{
		List<Value> currentValues = pCell.getPossibleValues().getValues();
		for (OneValueByCellConstraint constraint : pCell.getConstraints())
		{	
			if (currentValues.size() == 1)
			{
				for (Cell impacted : constraint.getConstrainedCells())
				{
					// prohibit this value to appear in constrained cells
					int uniqueValue = (int) currentValues.get(0).getValue();
					if (impacted != pCell)
					{
						ValueSet possibleValues = impacted.getPossibleValues();
						if (possibleValues.removeValue(uniqueValue))
						{
							addInOpen(impacted);
						}
					}
				}
			}
		}
	}

	private void evaluateQuick(Cell pCell)
	{
		List<Value> currentValues = pCell.getPossibleValues().getValues();
		List<Value> missing = pCell.getPossibleValues().complementary().getValues();
		for (OneValueByCellConstraint constraint : pCell.getConstraints())
		{	
			if (currentValues.size() == 1)
			{
				for (Cell impacted : constraint.getConstrainedCells())
				{
					// prohibit this value to appear in constrained cells
					int uniqueValue = (int) currentValues.get(0).getValue();
					if (impacted != pCell)
					{
						ValueSet possibleValues = impacted.getPossibleValues();
						if (possibleValues.removeValue(uniqueValue))
						{
							addInOpen(impacted);
						}
					}
				}
			}
			if (! missing.isEmpty() )
			{
				// there can be a missing value that exists in all by one Cell...
				// then remaining Cell should bear that value
				for (Value value : missing)
				{
					int possiblePlaces = 0;
					Cell lastPossiblePlace = null;
					for (Cell impacted : constraint.getConstrainedCells())
					{
						if (impacted != pCell)
						{
							if (impacted.getPossibleValues().hasValue(value.getValue()))
							{
								possiblePlaces ++;
								lastPossiblePlace = impacted;
							}
						}
						if (possiblePlaces > 1)
						{
							// useless to continue there are multiple possibilities...
							break;
						}
					}
					if (possiblePlaces == 1)
					{
						if (lastPossiblePlace.getPossibleValues().setValue(value.getValue()))
						{
							addInOpen(lastPossiblePlace);
						}
					}
				}				
			}
		}
	}

	/**
	 * Return all constraint having common cells with pOther excluding toExclude
	 * @param pOther
	 * @param toExclude
	 * @return
	 */
	private HashSet<OneValueByCellConstraint> intersect(OneValueByCellConstraint pOther, Cell toExclude)
	{
		HashSet<OneValueByCellConstraint> inter = null;
		for (Cell impacted : pOther.getConstrainedCells())
		{
			if (impacted != toExclude)
			{
				if (inter == null)
				{
					inter = new HashSet<OneValueByCellConstraint>();
				}
				for (OneValueByCellConstraint c : impacted.getConstraints())
				{
					if (c != pOther)
					{
						inter.add(c);
					}
				}
			}
		}
		return inter;
	}

    /** what is the difference between evaluate missing 1 and evaluate missing 2 ? */
	private void evaluateMissing1(Cell pCell)
	{
		for (OneValueByCellConstraint constraint : pCell.getConstraints())
		{	
			HashSet<OneValueByCellConstraint> inter = intersect(constraint,null);
			for (OneValueByCellConstraint intersect : inter)
			{
				ValueSet unionSet = new ValueSet(9,true);
				// ValueSet interSet = new ValueSet(9);
				for (Cell c : constraint.getConstrainedCells())
				{
					if (! c.getConstraints().contains(intersect))
					{
						unionSet.or(c.getPossibleValues());
						// interSet.and(c.getPossibleValues());
					}
				}
				ValueSet complementary = unionSet.complementary();
				if (! complementary.getValues().isEmpty())
				{
					List<Cell> possibleValues = new ArrayList<Cell>();
					for (Cell c : intersect.getConstrainedCells())
					{
						if (c != pCell)
						{
							if (! c.getConstraints().contains(constraint))
							{
								if (c.getPossibleValues().and(unionSet))
								{
									addInOpen(c);
								}
							}
							else
							{
								possibleValues.add(c);
							}
						}
					}
					if (possibleValues.size() == 1)
					{
						Cell impacted = possibleValues.get(0);
						if (impacted.getPossibleValues().and(complementary))
						{
							addInOpen(impacted);
						}
					}
				}
			}
		}
	}

    /** what is the difference between evaluate missing 1 and evaluate missing 2 ? */
	private void evaluateMissing2(Cell pCell)
	{
		List<Value> missing = pCell.getPossibleValues().complementary().getValues();
		for (OneValueByCellConstraint constraint : pCell.getConstraints())
		{	
			if (! missing.isEmpty() )
			{
				// there can be a missing value that exists in all by one Cell...
				// then remaining Cell should bear that value
				for (Value value : missing)
				{
					List<Cell> possiblePlace = new ArrayList<Cell>();
					// constraint existing in all cell excepting current evaluated constraint
					List<OneValueByCellConstraint> sharedConstraints = null;
					for (Cell impacted : constraint.getConstrainedCells())
					{
						if (impacted != pCell)
						{
							if (impacted.getPossibleValues().hasValue(value.getValue()))
							{
								possiblePlace.add(impacted);
								if (sharedConstraints == null)
								{
									sharedConstraints = new ArrayList<OneValueByCellConstraint>();
									for (OneValueByCellConstraint c : impacted.getConstraints())
									{
										if (c != constraint)
										{
											sharedConstraints.add(c);
										}
									}
								}
								else
								{
									List<OneValueByCellConstraint> newConstraints =  new ArrayList<OneValueByCellConstraint>();
									for (OneValueByCellConstraint shared : sharedConstraints)
									{
										if (impacted.getConstraints().contains(shared))
										{
											newConstraints.add(shared);
										}
									}
									sharedConstraints = newConstraints;
								}
							}
						}
					}
					if ( ! possiblePlace.isEmpty() )
					{
						System.out.println("Shared constraints : " + sharedConstraints );
						for (OneValueByCellConstraint shared : sharedConstraints)
						{
							for (Cell other : shared.getConstrainedCells() )
							{
								if (! possiblePlace.contains(other))
								{
									System.out.println("XXX " + value  +  " " + other);
									if ( other.getPossibleValues().removeValue(value.getValue()))
									{
										addInOpen(other);
									}
								}
							}
						}
						if (possiblePlace.size()==1)
						{
							Cell lastPossiblePlace = possiblePlace.get(0);
							if (lastPossiblePlace.getPossibleValues().setValue(value.getValue()))
							{
								addInOpen(lastPossiblePlace);
							}
						}
					}
					else
					{
						// this is an impossiblity => error
						System.out.println("No solution due to " + pCell + " value " + value);
					}
				}				
			}
		}
	}

	private void addInOpen(Cell pCell)
	{
	    int length = pCell.getPossibleValues().getValues().size();
	    System.out.println("length : " + length );
	    if ( length < priorityQueues.length )
		{
		    if ( priorityQueues[length] == null )
			{
			    priorityQueues[length] = new Queue();
			}
		    priorityQueues[length].enqueue(pCell);
		}
	}
	
	private Cell removeFromOpen()
	{
	    return removeFromOpen(null);
	}

        private Cell removeFromOpen(SelectStrategy strategy)
        {
	    // walk all queues in priority order
	    Cell found = null;

	    for (Queue queue : priorityQueues)
		{
		    if (queue != null)
			{
			    found = queue.dequeue();
			    if ( found != null )
				{
				    return found;
				}
			}
		}
	    return found;
	}
	
		public static void main(String pArgs[])
		{
			final Table sudoku = new Table(9);
			
			
//			Schedule a job for the event-dispatching thread:
	        //creating and showing this application's GUI.
	        javax.swing.SwingUtilities.invokeLater(new Runnable() {
	            public void run() {
	    			SudokuGui s = new SudokuGui(sudoku);
	            }
	        });
	        
	        /*
	        try
	        {
	        while (true)
	        {
	        	synchronized(sudoku)
	        	{
	        		sudoku.wait(100);
	        	}
	        }
	        }
	        catch(InterruptedException ie)
	        {
	        	System.out.println("interrupted !");
	        }
	        */

			/*
			List<Column> columns = sudoku.getColumns();
			List<Row> rows = sudoku.getRows();
			for (Column column : columns)
			{
				for (Row row : rows)
				{
					if (row.getIndex() == column.getIndex())
					{
						Cell cell = sudoku.get(row,column); 
						cell.getPossibleValues().setValue(row.getIndex());
						// cell.getPossibleValues().addValue(column.getIndex());
						
						System.out.println(sudoku); 
	
						SudokuResolver solver = new SudokuResolver(cell);
						solver.evaluateAll();
						System.out.println(sudoku);
					}
				}
			}
			*/

		}
}
