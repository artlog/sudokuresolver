#!/bin/bash

ant

test=$(java -cp dist/lib/sudokuresolver-0.1.0.jar org.lhardy.sudoku.SudokuReader)

echo "verify:"
echo "$test"

java -cp dist/lib/sudokuresolver-0.1.0.jar org.lhardy.sudoku.SudokuReader "$test"

java -cp dist/lib/sudokuresolver-0.1.0.jar org.lhardy.sudoku.SudokuReader "row:0SU"
